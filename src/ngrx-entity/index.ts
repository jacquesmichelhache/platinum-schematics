import { apply, chain, MergeStrategy, mergeWith, move, Rule, SchematicContext, template, Tree, url } from '@angular-devkit/schematics';
import { normalize } from 'path';
import { strings } from '@angular-devkit/core';
import { setupOptions } from '../utils/utility';
import { InsertChange } from '@schematics/angular/utility/change';
import { createAddEntityToReducersArrayContext } from './utils/add-entity-to-reducers-array';
import { addReducerImportDeclarationChange } from './utils/add-reducer-import-declaration.change';
import { addEntitySelectorsChange } from './utils/add-entity-selectors.change';
import { addEntityToReducersArrayChange } from './utils/add-entity-to-reducers-array.change';
import { addEffectToArrayChange } from './utils/add-entity-effect-to-array.change';
import { addEffectsImportDeclarationChange } from './utils/add-effects-import-declaration.change';


export function ngrxEntity(_options: any): Rule {
  return async (tree: Tree, _context: SchematicContext) => {
    await setupOptions(tree,_options)

    // create action/class/reducer/effect/service for the given entity
    const movePath = normalize(_options.path + '/app/entities')
    const templateSource = apply(url('./files'), [
      template({ ..._options, ...strings}),
      move(movePath)
    ])

    //context
    let context = createAddEntityToReducersArrayContext(_options)
    context.path = 'src/app/entities/reducers.ts'
    const ReducersChangeRecorder = tree.beginUpdate(context.path)

    // step 1 update reducers.ts: imports
    let change = addReducerImportDeclarationChange(context, tree)   
    if (change instanceof InsertChange){
      ReducersChangeRecorder.insertLeft(change.pos, change.toAdd)
    }

    // step 2 update reducers.ts: reducers Array   
    let change1 = addEntityToReducersArrayChange(context, tree)
    if (change1 instanceof InsertChange){
      ReducersChangeRecorder.insertLeft(change1.pos, change1.toAdd)
    }

    // step 3 update reducers.ts: entity selectors
    let change2 = addEntitySelectorsChange(context, tree)
    if (change2 instanceof InsertChange){
      ReducersChangeRecorder.insertLeft(change2.pos, change2.toAdd)
    }

     // step 4 update effects.ts: add import declaration
     context.path = 'src/app/entities/effects.ts'
     const EffectsChangeRecorder = tree.beginUpdate(context.path)
     let change4 = addEffectsImportDeclarationChange(context, tree)
     if (change4 instanceof InsertChange){
       EffectsChangeRecorder.insertLeft(change4.pos, change4.toAdd)
     }

    // step 5 update effects.ts: add entity effect
    let change3 = addEffectToArrayChange(context, tree)
    if (change3 instanceof InsertChange){
      EffectsChangeRecorder.insertLeft(change3.pos, change3.toAdd)
    }

    // commit changes
    tree.commitUpdate(ReducersChangeRecorder)
    tree.commitUpdate(EffectsChangeRecorder)
    
    return chain([mergeWith(templateSource, MergeStrategy.Default)]);
  };
}




// function showTree(node: ts.Node, indent: string = '    '): void {
//  // will output the syntax kind of the node
//  console.log(indent + ts.SyntaxKind[node.kind]);
//  // output the text of node
//  if (node.getChildCount() === 0) {
//    console.log(indent + '    Text: ' + node.getText());
//  }

//  // output the children nodes
//  for (let child of node.getChildren()) {
//    showTree(child, indent + '    ');
//  }
// }


// let buffer = fs.readFileSync('src/app/entities/reducers.ts');
// let content = buffer.toString('utf-8');
// // create a typescript source file out of demo.ts
// let node = ts.createSourceFile('demo.ts', content, ts.ScriptTarget.Latest, true);
// showTree(node);