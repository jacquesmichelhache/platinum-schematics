import * as actions from './actions'
import { EntitiesState } from '../entities-state'
import { <%= capitalize(entity_singular)%> } from './classes'

export const initialState: EntitiesState<<%= capitalize(entity_singular)%>> = {
  page: null,
  page_size: null,
  total: null,
  data: [],
  selected: null,
  loading: false,
  saveStatus: 'none',
}

export function reducer(state = initialState, action: actions.Actions): EntitiesState<<%= capitalize(entity_singular)%>> {
  switch (action.type) {
    case actions.CLEAR_<%= underscore(entity_plural).toUpperCase() %>:
      return { ...initialState }
    case actions.UNSELECT_<%= underscore(entity_singular).toUpperCase() %>:
      return {
        ...state,
        selected: null,
        loading: false,
        saveStatus: 'none',
      }
    case actions.SELECT_<%= underscore(entity_singular).toUpperCase() %>:
      return {
        ...state,
        selected: action.object,
        loading: false,
      }
    case actions.GET_<%= underscore(entity_singular).toUpperCase() %>:
      return {
        ...state,
        selected: null,
        loading: true,
      }
    case actions.LOAD_<%= underscore(entity_plural).toUpperCase() %>:
    case actions.CREATE_<%= underscore(entity_singular).toUpperCase() %>:
    case actions.UPDATE_<%= underscore(entity_singular).toUpperCase() %>:
    case actions.DELETE_<%= underscore(entity_singular).toUpperCase() %>:
      return {
        ...state,
        loading: true,
        saveStatus: 'none',
      }
    case actions.ACTION_DONE:
      return {
        ...state,
        ...(action?.options?.skipStateChange ? {} : action.newState),
        loading: false,
      }
    default:
      return state
  }
}

export const get<%= capitalize(entity_plural)%> = (state: EntitiesState<<%= capitalize(entity_singular)%>>) => state.data
export const getSelected<%= capitalize(entity_singular)%> = (state: EntitiesState<<%= capitalize(entity_singular)%>>) => state.selected
export const isLoading = (state:EntitiesState<<%= capitalize(entity_singular)%>>) => state.loading
