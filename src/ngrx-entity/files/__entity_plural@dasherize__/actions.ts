import { Action } from '@ngrx/store'
import { entityOptions } from '../shared/classes'
import { <%= capitalize(entity_singular)%> } from './classes'


export const LOAD_<%= underscore(entity_plural).toUpperCase() %> = '[<%= capitalize(entity_plural)%>] Load <%= capitalize(entity_plural)%>'
export const UNSELECT_<%= underscore(entity_singular).toUpperCase() %> = '[<%= capitalize(entity_plural)%>] Unselect <%= capitalize(entity_singular)%>'
export const SELECT_<%= underscore(entity_singular).toUpperCase() %> = '[<%= capitalize(entity_plural)%>] Select <%= capitalize(entity_singular)%>'
export const GET_<%= underscore(entity_singular).toUpperCase() %> = '[<%= capitalize(entity_plural)%>] Get <%= capitalize(entity_singular)%>'
export const CREATE_<%= underscore(entity_singular).toUpperCase() %> = '[<%= capitalize(entity_plural)%>] Create <%= capitalize(entity_singular)%>'
export const UPDATE_<%= underscore(entity_singular).toUpperCase() %> = '[<%= capitalize(entity_plural)%>] Update <%= capitalize(entity_singular)%>'
export const DELETE_<%= underscore(entity_singular).toUpperCase() %> = '[<%= capitalize(entity_plural)%>] Delete <%= capitalize(entity_singular)%>'
export const CLEAR_<%= underscore(entity_plural).toUpperCase() %> = '[<%= capitalize(entity_plural)%>] Clear <%= capitalize(entity_plural)%>'
export const ACTION_DONE = '[<%= capitalize(entity_plural)%>] Action Done'


export interface <%= capitalize(entity_plural)%>EntityOptions extends entityOptions<<%= capitalize(entity_singular)%>> {
  // add extra options here
}

const default<%= capitalize(entity_plural)%>Options:  <%= capitalize(entity_plural)%>EntityOptions = {
  // extra option default values
}

export class Load<%= capitalize(entity_plural)%> implements Action {
  readonly type = LOAD_<%= underscore(entity_plural).toUpperCase() %>
  constructor(public params: any = {}, public options: Partial<<%= capitalize(entity_plural)%>EntityOptions>  = default<%= capitalize(entity_plural)%>Options) {}
}

export class Unselect<%= capitalize(entity_singular)%> implements Action {
  readonly type = UNSELECT_<%= underscore(entity_singular).toUpperCase() %>
  constructor() {}
}

export class Select<%= capitalize(entity_singular)%> implements Action {
  readonly type = SELECT_<%= underscore(entity_singular).toUpperCase() %>
  constructor(public object: <%= capitalize(entity_singular)%>) {}
}

export class Get<%= capitalize(entity_singular)%> implements Action {
  readonly type = GET_<%= underscore(entity_singular).toUpperCase() %>
  constructor(public id: number, public options: Partial<<%= capitalize(entity_plural)%>EntityOptions>  = default<%= capitalize(entity_plural)%>Options) {}
}

export class Create<%= capitalize(entity_singular)%> implements Action {
  readonly type = CREATE_<%= underscore(entity_singular).toUpperCase() %>
  constructor(public data: Partial<<%= capitalize(entity_singular)%>>, public options: Partial<<%= capitalize(entity_plural)%>EntityOptions> = default<%= capitalize(entity_plural)%>Options) {}
}

export class Update<%= capitalize(entity_singular)%> implements Action {
  readonly type = UPDATE_<%= underscore(entity_singular).toUpperCase() %>
  constructor(public id: number, public data: <%= capitalize(entity_singular)%> | any, public options: Partial<<%= capitalize(entity_plural)%>EntityOptions>  = default<%= capitalize(entity_plural)%>Options) {}
}

export class Delete<%= capitalize(entity_singular)%> implements Action {
  readonly type = DELETE_<%= underscore(entity_singular).toUpperCase() %>
  constructor(public id: number, public options: Partial<<%= capitalize(entity_plural)%>EntityOptions>  = default<%= capitalize(entity_plural)%>Options) {}
}

export class ActionDone implements Action {
  readonly type = ACTION_DONE
  constructor(public newState: any = {}, public options: Partial<<%= capitalize(entity_plural)%>EntityOptions>  = default<%= capitalize(entity_plural)%>Options) {}
}

export class Clear<%= capitalize(entity_plural)%> implements Action {
  readonly type = CLEAR_<%= underscore(entity_plural).toUpperCase() %>
  constructor() {}
}

export type Actions = 
Load<%= capitalize(entity_plural)%>
| Unselect<%= capitalize(entity_singular)%>
| Select<%= capitalize(entity_singular)%>
| Get<%= capitalize(entity_singular)%>
| Create<%= capitalize(entity_singular)%>
| Delete<%= capitalize(entity_singular)%>
| Clear<%= capitalize(entity_plural)%>
| Update<%= capitalize(entity_singular)%>
| ActionDone 
