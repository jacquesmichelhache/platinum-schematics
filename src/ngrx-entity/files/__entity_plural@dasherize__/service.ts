import { Observable, ReplaySubject } from 'rxjs'
import { IndexResponse } from '../index-response'
import { <%= capitalize(entity_singular)%> } from './classes'
import { setHttpParams } from 'src/app/helpers/http-helpers'
import { Injectable } from '@angular/core'
import { BaseEntityService } from '../base-entity-service'
import { Store } from '@ngrx/store'
import { Actions, ofType } from '@ngrx/effects'
import { EntityServiceBase } from '../shared/entity-service-base'
import * as <%= capitalize(entity_plural)%>Actions from './actions'
import { filter, map, take } from 'rxjs/operators'
import { <%= capitalize(entity_plural)%>EntityOptions } from './actions'

// data service. This is where http requests are sent to API
@Injectable({ providedIn: 'root' })
export class <%= capitalize(entity_plural)%>DataService extends BaseEntityService {
  get<%= capitalize(entity_plural)%>$(params?: object): Observable<IndexResponse<<%= capitalize(entity_singular)%>>> {
    return this.http.get<IndexResponse<<%= capitalize(entity_singular)%>>>(`${this.tenantApiUrl}/<%= underscore(entity_plural).toLowerCase() %>`, { params: setHttpParams(params) })
  }

  get<%= capitalize(entity_singular)%>$(id: number): Observable<<%= capitalize(entity_singular)%>> {
    return this.http.get<<%= capitalize(entity_singular)%>>(`${this.tenantApiUrl}/<%= underscore(entity_plural).toLowerCase() %>/${id}`)
  }

  create<%= capitalize(entity_singular)%>$(data: Partial<<%= capitalize(entity_singular)%>> | FormData): Observable<<%= capitalize(entity_singular)%>> {
    return this.http.post<<%= capitalize(entity_singular)%>>(`${this.tenantApiUrl}/<%= underscore(entity_plural).toLowerCase() %>`, { <%= underscore(entity_singular).toLowerCase() %>: data })
  }

  update<%= capitalize(entity_singular)%>$(id: number, data: Partial<<%= capitalize(entity_singular)%>> | FormData): Observable<<%= capitalize(entity_singular)%>> {
    return this.http.patch<<%= capitalize(entity_singular)%>>(`${this.tenantApiUrl}/<%= underscore(entity_plural).toLowerCase() %>/${id}`, { <%= underscore(entity_singular).toLowerCase() %>: data })
  }

  delete<%= capitalize(entity_singular)%>$(id: number): Observable<<%= capitalize(entity_singular)%>> {
    return this.http.delete<<%= capitalize(entity_singular)%>>(`${this.tenantApiUrl}/<%= underscore(entity_plural).toLowerCase() %>/${id}`)
  }
}

// Entity operations service facad implementing correlationId
@Injectable({ providedIn: 'root' })
export class <%= capitalize(entity_plural)%>EntityOperationsService {
  constructor(private rxStore: Store, private actions$: Actions, private entityServiceBase: EntityServiceBase) {}

  private result<T extends <%= capitalize(entity_singular)%> | <%= capitalize(entity_singular)%>[]>(corrId: string): Observable<T> {
    const replaySubject = new ReplaySubject<T>(1)
    this.actions$
      .pipe(
        ofType<<%= capitalize(entity_plural)%>Actions.ActionDone>(<%= capitalize(entity_plural)%>Actions.ACTION_DONE),
        filter(action => corrId === action?.options?.corrId),
        take(1),
      )
      .subscribe(action => {
        if (action.options.errors) {
          replaySubject.error(action.options.errors)
        } else {
          replaySubject.next(action.options.data as T)
          replaySubject.complete()
        }
      })
    return replaySubject
  }

  getAll(options: Partial<<%= capitalize(entity_plural)%>EntityOptions> = {}): Observable<<%= capitalize(entity_singular)%>[]> {
    const corrId = this.entityServiceBase.generateUniqueId()
    const result$ = this.result<<%= capitalize(entity_singular)%>[]>(corrId)
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Load<%= capitalize(entity_plural)%>(null, {  ...options, corrId: corrId }))
    return result$
  }

  getWithQuery(params: Object, options: Partial<<%= capitalize(entity_plural)%>EntityOptions> = {}): Observable<<%= capitalize(entity_singular)%>[]> {
    const corrId = this.entityServiceBase.generateUniqueId()
    const result$ = this.result<<%= capitalize(entity_singular)%>[]>(corrId)
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Load<%= capitalize(entity_plural)%>(params, {  ...options, corrId: corrId }))
    return result$
  }

  getById(id: number, options: Partial<<%= capitalize(entity_plural)%>EntityOptions> = {}): Observable<<%= capitalize(entity_singular)%>> {
    const corrId = this.entityServiceBase.generateUniqueId()
    const result$ = this.result<<%= capitalize(entity_singular)%>>(corrId)
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Get<%= capitalize(entity_singular)%>(id, {  ...options, corrId: corrId }))
    return result$
  }

  add(<%= capitalize(entity_singular)%>: Partial<<%= capitalize(entity_singular)%>>, options: Partial<<%= capitalize(entity_plural)%>EntityOptions> = {}): Observable<<%= capitalize(entity_singular)%>> {
    const corrId = this.entityServiceBase.generateUniqueId()
    const result$ = this.result<<%= capitalize(entity_singular)%>>(corrId)
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Create<%= capitalize(entity_singular)%>(<%= capitalize(entity_singular)%>, {  ...options, corrId: corrId }))
    return result$
  }

  update(id: number, <%= capitalize(entity_singular)%>: Partial<<%= capitalize(entity_singular)%>>, options: Partial<<%= capitalize(entity_plural)%>EntityOptions> = {}): Observable<<%= capitalize(entity_singular)%>> {
    const corrId = this.entityServiceBase.generateUniqueId()
    const result$ = this.result<<%= capitalize(entity_singular)%>>(corrId)
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Update<%= capitalize(entity_singular)%>(id, <%= capitalize(entity_singular)%>, {  ...options, corrId: corrId }))
    return result$
  }

  delete(id: number, options: Partial<<%= capitalize(entity_plural)%>EntityOptions> = {}): Observable<<%= capitalize(entity_singular)%>> {
    const corrId = this.entityServiceBase.generateUniqueId()
    const result$ = this.result<<%= capitalize(entity_singular)%>>(corrId)
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Delete<%= capitalize(entity_singular)%>(id, {  ...options, corrId: corrId }))
    return result$
  }
}
